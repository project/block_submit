(function ($) {
  $(document).ready(function() {
    $('form').each(function (i) {
      var form = $(this);
      $('input.form-submit', form).click(function (e) {
        var el = $(this);
        el.after('<input type="hidden" name="' + el.attr('name') + '" value="' + el.attr('value') + '" />');
        return true;
      });
    });

    $('form').submit(function (e) {
      var settings = Drupal.settings.block_submit;
      var inp;
      if (settings.block_submit_method == 'disable') {
        $('input.form-submit', $(this)).attr('disabled', 'disabled').each(function (i) {
          if (settings.block_submit_css) {
            $(this).addClass(settings.block_submit_css);
          }
          if (settings.block_submit_abtext) {
            $(this).val($(this).val() + ' ' + settings.block_submit_abtext);
          }
          inp = $(this);
        });
      
        if (inp && settings.block_submit_atext) {
          inp.after('<span>' + settings.block_submit_atext + '</span>');
        }
      }
      else {
        var pdiv = '<div' + (settings.block_submit_hide_css?' class="' + settings.block_submit_hide_css + '"':'') + '>' + settings.block_submit_hide_text + '</div>';
        if (settings.block_submit_hide_fx) {
          $('input.form-submit', $(this)).fadeOut(100).eq(0).after(pdiv);
          $('input.form-submit', $(this)).next().fadeIn(100);
        }
        else {
          $('input.form-submit', $(this)).css('display', 'none').eq(0).after(pdiv);
        }
      }
      return true;
    });
  }); 
})(jQuery);